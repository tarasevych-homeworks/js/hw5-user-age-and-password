
function createNewUser() {
// debugger;

    const userFirstName = prompt('Enter your first name:', 'Yevhenii');
    const userLastName = prompt('Enter your last name:', 'Tarasevych');
    let userBirthday = prompt('Enter your birthday', '04.12.1992');

    return newUser = {
        firstName: userFirstName,
        lastName: userLastName,
        birthday: userBirthday,

        getAge: function () {
            let thisYear = new Date().getFullYear();
            let day = +this.birthday.substr(0, 2);
            let month = +this.birthday.substr(3, 2);
            let year = +this.birthday.substr(6, 4);
            let birthDay = new Date(year, month-1, day-1);
            let birthYear = birthDay.getFullYear();
            return thisYear - birthYear;
        },

        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLocaleLowerCase()
        },

        getPassword: function () {
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6,10);}
    }
}

console.log(createNewUser());
console.log('User age: ' + newUser.getAge());
console.log('User login: ' + newUser.getLogin());
console.log('User password: ' + newUser.getPassword());
